
package services;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Activity;
import domain.Customer;
import domain.Gym;
import repositories.ActivityRepository;

@Service
@Transactional
public class ActivityService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ActivityRepository activityRepository;


	// Constructor methods --------------------------------------------------------------
	public ActivityService() {
		super();
	}


	// Supporting services --------------------------------------------------------------
	@Autowired
	private CustomerService customerService;

	@Autowired
	private GymService		gymService;
	// Simple CRUD methods --------------------------------------------------------------


	public Activity findOne(int activityId) {
		Assert.isTrue(activityId != 0);
		Activity result;

		result = activityRepository.findOne(activityId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Activity> findAll() {
		Collection<Activity> result;

		result = activityRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Activity activity) {
		Assert.notNull(activity);

		activityRepository.save(activity);
	}

	public Activity create() {

		Activity activity = new Activity();

		return activity;
	}

	public Activity saveAndFlush(Activity activity) {
		Assert.notNull(activity);

		return activityRepository.saveAndFlush(activity);
	}

	public Activity cancelActivity(Integer gymId, Integer activityId) {

		try {
			Activity activity = findOne(activityId);
			activity.setIsCanceled(true);
			return saveAndFlush(activity);
		} catch (Exception e) {
			return null;
		}
	}

	// Other bussines methods -----------------------------------------------------

	/**
	 * M�todo que a�ade el activity a la lista de activitys a los que est� apuntado
	 *
	 * @param activityId
	 * @return
	 */
	public boolean joinActivity(Integer activityId) {
		boolean exito = false;

		try {
			Activity activity = findOne(activityId);
			Assert.notNull(activity);

			Customer customer = customerService.findByPrincipal();
			Assert.notNull(customer);

			Assert.isTrue(activity.getGym().getCustomers().contains(customer));

			if (!activity.getCustomers().contains(customer)) {
				activity.getCustomers().add(customer);
				save(activity);
				//				if (!customer.getActivities().contains(activity)) {
				//					customer.getActivities().add(activity);
				//					customerService.save(customer);
				//				}
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}

	/**
	 * M�todo que quita el activity a la lista de activitys a los que est� apuntado
	 *
	 * @param activityId
	 * @return
	 */
	public boolean leaveActivity(Integer activityId) {
		boolean exito = false;

		try {
			Activity activity = findOne(activityId);
			Assert.notNull(activity);

			Customer customer = customerService.findByPrincipal();
			Assert.notNull(customer);

			Assert.isTrue(activity.getGym().getCustomers().contains(customer));

			if (activity.getCustomers().contains(customer)) {
				activity.getCustomers().remove(customer);
				save(activity);
			}
			exito = true;
		} catch (Exception e) {
			exito = false;
		}

		return exito;
	}

	public Collection<Activity> searchActivities(String keyword, Date startDate, Date endDate, Integer gymId, String dayWeek) {
		Collection<Activity> result = new ArrayList<Activity>();
		Collection<Activity> aux = new ArrayList<Activity>();

		if (keyword == null) {
			keyword = "";
		}
		if (dayWeek == null) {
			dayWeek = "";
		}

		try {
			if (gymId == null) {
				aux = activityRepository.getActivitiesByKeyword("%" + keyword + "%", "%" + dayWeek + "%");
			} else {
				Gym gym = this.gymService.findOne(gymId);
				Assert.notNull(gym);
				Assert.isTrue(gym.getIsDeleted() != true);
				aux = gym.getActivities();
			}

			Time startTime = null;
			if(startDate!=null){
				startTime = new Time(startDate.getTime());
			}
			Time endTime = null;
			if(endDate!=null){
				endTime = new Time(endDate.getTime());
			}

			//Filtrado de fechas
			for (Activity activity : aux)
				if (startTime != null && endTime != null) {
					//Si la fecha de la actividad est� dentro de las fechas de filtro
					if ((activity.getStartTime().after(startTime) || activity.getStartTime().equals(startTime)) && (activity.getEndTime().before(endTime) || activity.getEndTime().equals(endTime)))
						result.add(activity);
				} else if (startTime != null) {
					//Si la fecha de la actividad est� dentro de las fechas de filtro
					if (activity.getStartTime().after(startTime) || activity.getStartTime().equals(startTime))
						result.add(activity);
				} else if (endTime != null) {
					//Si la fecha de la actividad est� dentro de las fechas de filtro
					if (activity.getEndTime().before(endTime) || activity.getEndTime().equals(endTime))
						result.add(activity);
				} else
					result.add(activity);

		} catch (Exception e) {
			return new ArrayList<Activity>();
		}
		return result;
	}

	public Collection<Activity> findAllValid() {
		return activityRepository.findAllValid();
	}

}
