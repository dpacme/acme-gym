/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import services.GymService;

@Controller
@RequestMapping("/gym/customer")
public class GymCustomerController extends AbstractController {

	// Services

	@Autowired
	private GymService gymService;


	// Constructors -----------------------------------------------------------

	public GymCustomerController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------

	@RequestMapping(value = "/joinGym", method = RequestMethod.GET)
	public ModelAndView joinGym(@RequestParam Integer gymId) {
		ModelAndView result;
		boolean exito = this.gymService.joinGym(gymId);

		if (exito) {
			result = new ModelAndView("redirect:/gym/list.do");
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/leaveGym", method = RequestMethod.GET)
	public ModelAndView leaveGym(@RequestParam Integer gymId) {
		ModelAndView result;
		boolean exito = this.gymService.leaveGym(gymId);

		if (exito) {
			result = new ModelAndView("redirect:/gym/list.do");
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}
}
