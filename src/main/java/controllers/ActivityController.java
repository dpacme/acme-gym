/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Activity;
import domain.Gym;
import services.ActivityService;
import services.GymService;

@Controller
@RequestMapping("/activity")
public class ActivityController extends AbstractController {

	// Services

	@Autowired
	private ActivityService activityService;

	@Autowired
	private GymService		gymService;


	// Constructors -----------------------------------------------------------

	public ActivityController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Activity> activities = activityService.findAll();
		result = new ModelAndView("activity/list");
		result.addObject("activities", activities);
		result.addObject("requestURI", "activity/list.do");
		result.addObject("create", false);

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(String keyword, String dayWeek, Date startDate, Date endDate, Integer gymId) {
		return searchActivities(keyword, dayWeek, startDate, endDate, gymId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(String keyword, String dayWeek, Date startDate, Date endDate, Integer gymId) {
		return searchActivities(keyword, dayWeek, startDate, endDate, gymId);
	}

	private ModelAndView searchActivities(String keyword, String dayWeek, Date startDate, Date endDate, Integer gymId) {
		ModelAndView result;
		final Collection<Activity> res;

		res = activityService.searchActivities(keyword, startDate, endDate, gymId, dayWeek);

		if (gymId != null) {
			Gym gym = gymService.findOne(gymId);
			Assert.notNull(gym);
			Assert.isTrue(gym.getIsDeleted() != true);
		}

		result = new ModelAndView("activity/list");
		result.addObject("activities", res);
		result.addObject("requestURI", "activity/search.do");
		result.addObject("create", false);

		return result;
	}
	//yyyy/MM/dd HH:mm
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		CustomDateEditor editor = new CustomDateEditor(new SimpleDateFormat("HH:mm"), true);
		binder.registerCustomEditor(Date.class, editor);
	}
}
