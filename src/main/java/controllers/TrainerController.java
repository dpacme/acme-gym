/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Activity;
import domain.Trainer;
import services.ActivityService;
import services.TrainerService;

@Controller
@RequestMapping("/trainer")
public class TrainerController extends AbstractController {

	// Services

	@Autowired
	private ActivityService	activityService;

	@Autowired
	private TrainerService	trainerService;


	// Constructors -----------------------------------------------------------

	public TrainerController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	// List Method ------------------------------------------------

	@RequestMapping(value = "/listTrainersFromActivity", method = RequestMethod.GET)
	public ModelAndView listTrainers(@RequestParam final Integer activityId) {
		ModelAndView result;
		final Activity activity = this.activityService.findOne(activityId);
		result = new ModelAndView("trainer/list");
		result.addObject("trainers", activity.getTrainers());
		result.addObject("requestURI", "gym/listTrainers.do");

		return result;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Trainer trainer = this.trainerService.findByPrincipal();

		res = this.createFormModelAndView(trainer);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Trainer trainer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(trainer);
			System.out.println(binding.getAllErrors());
		} else
			try {

				final List<String> errores = this.trainerService.comprobacionEditarListErrores(trainer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(trainer);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					this.trainerService.save(trainer);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(trainer);
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Trainer trainer) {
		ModelAndView res;

		res = this.createFormModelAndView(trainer, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Trainer trainer, final String message) {
		ModelAndView res;

		res = new ModelAndView("trainer/edit");
		res.addObject("trainer", trainer);
		res.addObject("message", message);

		return res;
	}

}
