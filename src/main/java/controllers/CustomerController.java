/*
 * CustomerController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Customer;
import forms.ActorForm;
import services.CustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController extends AbstractController {

	// Services

	@Autowired
	private CustomerService customerService;


	// Constructors -----------------------------------------------------------

	public CustomerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de un customer
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final ActorForm actorForm = new ActorForm();

		res = this.createFormModelAndView(actorForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo customer
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final ActorForm actorForm, final BindingResult binding) {
		ModelAndView res;
		Customer customer;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(actorForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(actorForm.getPostalAddress()) && !actorForm.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(actorForm.getPhone()) && !actorForm.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {

				customer = customerService.reconstruct(actorForm);

				final List<String> errores = customerService.comprobacionEditarListErrores(customer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(actorForm);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					customerService.saveForm(customer);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(actorForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException"))
					res.addObject("duplicate", "duplicate");
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions"))
					res.addObject("terms", "terms");
				if (oops.getLocalizedMessage().contains("Passwords do not match"))
					res.addObject("pass", "pass");
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final ActorForm actorForm) {
		ModelAndView res;

		res = this.createFormModelAndView(actorForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final ActorForm actorForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("customer/create");
		res.addObject("actorForm", actorForm);
		res.addObject("message", message);

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		final Customer customer = customerService.findByPrincipal();

		res = this.createFormModelAndView(customer);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo candidate
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Customer customer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(customer);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(customer.getPostalAddress()) && !customer.getPostalAddress().matches("^(\\d{5}$)"))
				res.addObject("postal", "postal");
			if (!StringUtils.isEmpty(customer.getPhone()) && !customer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("phone", "phone");
		} else
			try {
				//this.customerService.comprobacionEditar(customer);
				final List<String> errores = customerService.comprobacionEditarListErrores(customer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(customer);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					customerService.save(customer);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(customer);
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(final Customer customer) {
		ModelAndView res;

		res = this.createFormModelAndView(customer, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final Customer customer, final String message) {
		ModelAndView res;

		res = new ModelAndView("customer/edit");
		res.addObject("customer", customer);
		res.addObject("message", message);

		return res;
	}

}
