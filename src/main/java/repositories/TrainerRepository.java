
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Trainer;
import security.UserAccount;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Integer> {

	@Query("select a from Trainer a where a.userAccount = ?1")
	Trainer findByUserAccount(UserAccount userAccount);
}
