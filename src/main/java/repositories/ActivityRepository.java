
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {


	@Query("select a from Activity a where (a.title like ?1 or a.description like ?1) and a.gym.isDeleted = false and lower(a.dayWeek) like lower(?2)")
	Collection<Activity> getActivitiesByKeyword(String keyword, String dayWeek);

	@Query("select a from Activity a where a.gym.isDeleted = false")
	Collection<Activity> findAllValid();

}