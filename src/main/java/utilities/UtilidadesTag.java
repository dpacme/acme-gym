package utilities;

import java.util.Collection;

import javax.servlet.jsp.tagext.SimpleTagSupport;

import domain.Customer;
import security.UserAccount;

public class UtilidadesTag extends SimpleTagSupport {

	public static boolean contains(Collection<?> coll, Object o) {
		return coll.contains(o);
	}

	/**
	 * M�todo que recibe una collection de Customer y un UserAccount. Comprueba si alguno de los Customer de la collection tiene
	 * esa UserAccount
	 *
	 * @param coll
	 * @param o
	 * @return
	 */
	public static boolean listCustomerContainsUserAccount(Collection<Customer> coll, UserAccount o) {
		boolean contiene = false;
		for (Customer c : coll) {
			if (c.getUserAccount().getId() == o.getId()) {
				contiene = true;
				break;
			}
		}
		return contiene;
	}

}