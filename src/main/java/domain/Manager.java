
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Manager extends Actor {

	private Boolean isBanned;


	@NotNull
	public Boolean getIsBanned() {
		return this.isBanned;
	}

	public void setIsBanned(final Boolean isBanned) {
		this.isBanned = isBanned;
	}


	// Relationships -------------------------------------------

	private Collection<Gym> gyms;


	@Valid
	@OneToMany(mappedBy = "manager")
	public Collection<Gym> getGyms() {
		return this.gyms;
	}

	public void setGyms(final Collection<Gym> gyms) {
		this.gyms = gyms;
	}

}
