
package domain;

import java.net.URL;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Activity extends DomainEntity {

	private String			title;
	private Collection<URL>	pictures;
	private String			description;
	private String			dayWeek;
	private Date			startTime;
	private Date			endTime;
	private Integer			seatsAvailable;
	private Boolean			isCanceled;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotEmpty
	@ElementCollection
	public Collection<URL> getPictures() {
		return pictures;
	}

	public void setPictures(final Collection<URL> pictures) {
		this.pictures = pictures;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(monday|Monday|tuesday|Tuesday|wednesday|Wednesday|thursday|Thursday|friday|Friday|saturday|Saturday|sunday|Sunday)$")
	public String getDayWeek() {
		return dayWeek;
	}

	public void setDayWeek(final String dayWeek) {
		this.dayWeek = dayWeek;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:mm")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(final Date startTime) {
		this.startTime = startTime;
	}

	@NotNull
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:mm")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(final Date endTime) {
		this.endTime = endTime;
	}

	@Min(0)
	@NotNull
	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(final Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	@NotNull
	public Boolean getIsCanceled() {
		return isCanceled;
	}

	public void setIsCanceled(final Boolean isCanceled) {
		this.isCanceled = isCanceled;
	}


	// Relationships -------------------------------------------

	private Gym						gym;
	private Collection<Trainer>		trainers;
	private Collection<Customer>	customers;


	@Valid
	@ManyToOne(optional = false)
	public Gym getGym() {
		return gym;
	}

	public void setGym(final Gym gym) {
		this.gym = gym;
	}

	@Valid
	@ManyToMany
	public Collection<Trainer> getTrainers() {
		return trainers;
	}

	public void setTrainers(final Collection<Trainer> trainers) {
		this.trainers = trainers;
	}

	@Valid
	@ManyToMany
	public Collection<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(final Collection<Customer> customers) {
		this.customers = customers;
	}


	//Propiedades derivadas

	public boolean plazasDisponibles;


	/**
	 * M�todo de propiedad derivada que comprueba si quedan plazas disponibles
	 *
	 * @return
	 */
	@Transient
	public boolean isPlazasDisponibles() {
		return getSeatsAvailable() > getCustomers().size();
	}

}
