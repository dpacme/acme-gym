<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jstl:if test="${requestURI.contains('all') || requestURI.contains('search.do')}">
	<form method="POST" action="managerUri/trainer/search.do">
	
		<spring:message code="trainer.keyword" var="keywordPlaceholder"/>
		<input type="text" id="keyword" name="keyword" placeholder="${keywordPlaceholder}"/> 
		
		<input type="submit" name="search" value="<spring:message code="trainer.search"/>" />
	</form>
</jstl:if>

<jstl:if test="${requestURI.contains('listTrainers') || requestURI.contains('searchByGym.do')}">
	<form method="POST" action="managerUri/trainer/searchByGym.do?gymID=${gymID}">
	
		<spring:message code="trainer.keyword" var="keywordPlaceholder"/>
		<input type="text" id="keyword" name="keyword" placeholder="${keywordPlaceholder}"/> 
		
		<input type="submit" name="search" value="<spring:message code="trainer.search"/>" />
	</form>
</jstl:if>

<display:table name="trainers" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="trainer.name" var="name" />
	<display:column property="name" title="${name}" sortable="true"/>
	
	<spring:message code="trainer.surname" var="surname" />
	<display:column property="surname" title="${surname}" sortable="true"/>
	
	<spring:message code="trainer.email" var="email" />
	<display:column property="email" title="${email}" sortable="true"/>
	
	<spring:message code="trainer.phone" var="phone" />
	<display:column property="phone" title="${phone}" sortable="true"/>
	
	<spring:message code="trainer.postalAddress" var="postalAddress" />
	<display:column property="postalAddress" title="${postalAddress}" sortable="true"/>
	
	<spring:message code="trainer.city" var="city" />
	<display:column property="city" title="${city}" sortable="true"/>
	
	<spring:message code="trainer.country" var="country" />
	<display:column property="country" title="${country}" sortable="true"/>
	
	<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />
	
	<security:authorize access="hasRole('MANAGER')">
		<display:column>
			<jstl:if test="${row.getGym()==null}">
				<acme:button href="managerUri/trainer/associateTrainerToGym.do?trainerId=${row.id}"
					name="associateGym" code="trainer.associateGym" />
			</jstl:if>
		</display:column>
	</security:authorize>
	
</display:table>

<br/>
