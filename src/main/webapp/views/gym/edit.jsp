<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="gym/manager/edit.do" modelAttribute="gym">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="isDeleted" />
<form:hidden path="manager" />

	<acme:textbox code="gym.logo" path="logo" />
	<acme:textbox code="gym.name" path="name" />
	<acme:textbox code="gym.address" path="address" />
	
	<form:label path="fee">
		<spring:message code="gym.fee" />
	</form:label>
	<b></b>
	<form:input type="number" path="fee" />
	<form:errors class="error" path="fee" />
	<br>


	<input type="submit" name="save"
		value="<spring:message code="gym.save" />" />&nbsp; 

	<jstl:if test="${gym.id != 0}">
		<input type="submit" name="delete"
			value="<spring:message code="gym.delete" />"
			onclick="return confirm('<spring:message code="gym.confirm.delete" />')" />&nbsp;
	</jstl:if>

	<input type="button" name="cancel"
		value="<spring:message code="gym.cancel" />"
		onclick="javascript: window.location.replace('gym/manager/myGyms.do');" />
	<br />

</form:form>

