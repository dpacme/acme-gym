<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="utilidades" uri="/WEB-INF/tld/utilidades.tld"%>

<script>

$(function() {
   
    $('.datetimepicker').timepicker();
    
  });
</script>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

	<fieldset style="width: 20%">
		<legend>
			<spring:message code="activity.search" />
		</legend>
		<form method="POST" action="activity/search.do">

			<spring:message code="activity.keyword" />
			<spring:message code="activity.keyword" var="keywordPlaceholder" />
			<input type="text" id="keyword" name="keyword"
				placeholder="${keywordPlaceholder}" /> <br />
				
			<spring:message code="activity.dayWeek" />
			<select id="dayWeek" name="dayWeek" style="margin-bottom: 10px">
				<option value=""><spring:message code="activity.dayWeek.any"/></option>
				<option value="Monday"><spring:message code="activity.dayWeekMonday"/></option>
				<option value="Tuesday"><spring:message code="activity.dayWeekTuesday"/></option>
				<option value="Wednesday"><spring:message code="activity.dayWeekWednesday"/></option>
				<option value="Thursday"><spring:message code="activity.dayWeekThursday"/></option>
				<option value="Friday"><spring:message code="activity.dayWeekFriday"/></option>
				<option value="Saturday"><spring:message code="activity.dayWeekSaturday"/></option>
				<option value="Sunday"><spring:message code="activity.dayWeekSunday"/></option>
			</select>
<br />
			<spring:message code="activity.date.click" var="datePlaceholder" />
			<spring:message code="activity.startDate" />
			: <input type="text" id="startDate" name="startDate"
				readonly="readonly" placeholder="${datePlaceholder}"
				class="datetimepicker" /> <br />

			<spring:message code="activity.endDate" />
			: <input type="text" id="endDate" name="endDate" readonly="readonly"
				placeholder="${datePlaceholder}" class="datetimepicker" /> <br />
				
			<input type="hidden" id="gymId" name="gymId" value="${gym.id}" readonly="readonly" />
			
			<input type="submit" name="search"
				value="<spring:message code="activity.search"/>" />
		</form>
	</fieldset>

<security:authorize access="hasRole('MANAGER')">

	<jstl:if test="${create}">
		<div>
			<H5>
				<a href="activity/manager/create.do?gymId=${gym.id}"> <spring:message
						code="activity.create" />
				</a>
			</H5>
		</div>
	</jstl:if>
</security:authorize>

<display:table pagesize="5" class="displaytag" keepStatus="true"
	name="activities" requestURI="${requestURI}" id="row">

	<!-- Attributes -->

	<spring:message code="activity.pictures" var="pictureHeader" />
	<display:column title="${pictureHeader}">
		<jstl:forEach var="picture" items="${row.pictures}">
			<a href="${picture}" target="_blank"><img src="${picture}"
				height="64" width="64"></a>
		</jstl:forEach>
	</display:column>

	<spring:message code="activity.title" var="title" />
	<display:column property="title" title="${title}" sortable="true" />

	<spring:message code="activity.description" var="description" />
	<display:column property="description" title="${description}"
		sortable="true" />

	<spring:message code="activity.dayWeek" var="dayWeek" />
	<display:column title="${dayWeek}" sortable="true">
		<jstl:if test="${row.dayWeek == 'monday' || row.dayWeek == 'Monday'}">
			<spring:message code="activity.dayWeek.monday" />
		</jstl:if>
		<jstl:if
			test="${row.dayWeek == 'tuesday' || row.dayWeek == 'Tuesday'}">
			<spring:message code="activity.dayWeek.tuesday" />
		</jstl:if>
		<jstl:if
			test="${row.dayWeek == 'wednesday' || row.dayWeek == 'Wednesday'}">
			<spring:message code="activity.dayWeek.wednesday" />
		</jstl:if>
		<jstl:if
			test="${row.dayWeek == 'thursday' || row.dayWeek == 'Thursday'}">
			<spring:message code="activity.dayWeek.thursday" />
		</jstl:if>
		<jstl:if test="${row.dayWeek == 'friday' || row.dayWeek == 'Friday'}">
			<spring:message code="activity.dayWeek.friday" />
		</jstl:if>
		<jstl:if
			test="${row.dayWeek == 'saturday' || row.dayWeek == 'Saturday'}">
			<spring:message code="activity.dayWeek.saturday" />
		</jstl:if>
		<jstl:if test="${row.dayWeek == 'sunday' || row.dayWeek == 'Sunday'}">
			<spring:message code="activity.dayWeek.sunday" />
		</jstl:if>
	</display:column>

	<spring:message code="activity.startTime" var="startTime" />

	<display:column property="startTime" title="${startTime}" 
	format="{0,date,HH:mm}" sortable="true" />

	<spring:message code="activity.endTime" var="endTime" />
	<display:column property="endTime" title="${endTime}"
		format="{0,date,HH:mm}" sortable="true" />
	
	<spring:message code="activity.seatsAvailable" var="seatsAvailable" />
	<display:column property="seatsAvailable" title="${seatsAvailable}"
		sortable="true" />

	<spring:message code="activity.isCanceled" var="isCanceledHeader" />
	<display:column title="${isCanceledHeader}">
		<jstl:choose>
			<jstl:when test="${row.isCanceled == false}">
				<div>
					<spring:message code="activity.isCanceledNo" />
				</div>
			</jstl:when>
			<jstl:otherwise>
				<div>
					<spring:message code="activity.isCanceledYes" />
				</div>
			</jstl:otherwise>
		</jstl:choose>
	</display:column>

	<spring:message code="activity.gym" var="gymHeader" />
	<display:column title="${gymHeader}">
		<jstl:choose>
			<jstl:when test="${row.gym.isDeleted == true}">
				<spring:message code="gym.info.deleted"/>
			</jstl:when>
			<jstl:otherwise>
				<div>
					<input type="button" name="${showGym}" value="${row.gym.name}"
						onclick="javascript: window.location.replace('gym/show.do?gymId=${row.gym.id}');" />
				</div>	
			</jstl:otherwise>
		</jstl:choose>
		
		
	</display:column>

	<security:authorize access="hasRole('MANAGER')">

		<spring:message code="activity.isCanceledD" var="isCanceledDHeader" />
		<display:column title="${isCanceledDHeader}">
			<jstl:if
				test="${row.getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
				<jstl:if test="${row.isCanceled == false}">
					<form action="activity/manager/cancelActivity.do?" method="post">
						<input type="hidden" name="gymId" value="${gym.id}" /> <input
							type="hidden" name="activityId" value="${row.id}" /> <input
							type="submit" value="<spring:message code="activity.cancel" />" />
					</form>
				</jstl:if>
			</jstl:if>

		</display:column>

	</security:authorize>


	<spring:message code="gym.listTrainers" var="listTrainersHeader" />
	<display:column title="${listTrainersHeader}">

		<acme:button
			href="trainer/listTrainersFromActivity.do?activityId=${row.id}"
			name="listGym" code="gym.listTrainers.see" />

	</display:column>

	<security:authorize access="hasRole('MANAGER')">

		<jstl:if
			test="${row.getGym().getManager().getUserAccount().getId() == loginService.getPrincipal().getId()}">
			<display:column>
				<jstl:if test="${row.isCanceled == false}">
					<acme:button
						href="activity/manager/associateTrainer.do?activityID=${row.id}"
						name="associateTrainer" code="activity.associateTrainer" />
				</jstl:if>
			</display:column>
		</jstl:if>


	</security:authorize>

	<security:authorize access="hasRole('CUSTOMER')">
		<spring:message code="activity.customer.actions" var="actionsHeader" />
		<display:column title="${actionsHeader}">
			<!-- 			
				primer when comprueba si el customer est� unido al gym de la actividad
				segundo when comprueba si el customer est� apuntado a la actividad
			 -->
			<jstl:if test="${row.isCanceled == false}">
			<jstl:choose>
				<jstl:when
					test="${utilidades:listCustomerContainsUserAccount(row.gym.customers, loginService.getPrincipal()) == true}">
					<jstl:choose>
						<jstl:when
							test="${utilidades:listCustomerContainsUserAccount(row.customers, loginService.getPrincipal()) == true}">
									<div class="inline">
										<input type="button" href="#" disabled="disabled" name="ban"
											value='<spring:message code="activity.customer.actions.join" />' />
									</div>
									<div class="inline">
										<acme:button
											href="activity/customer/leaveActivity.do?activityId=${row.id}"
											name="unban" code="activity.customer.actions.leave" />
									</div>
						</jstl:when>
						<jstl:otherwise>
							<jstl:choose>
								<jstl:when
									test="${row.plazasDisponibles == true}">
									<div class="inline">
										<acme:button
											href="activity/customer/joinActivity.do?activityId=${row.id}"
											name="join" code="activity.customer.actions.join" />
									</div>
									<div class="inline">
										<input type="button" href="#" disabled="disabled" name="leave"
											value='<spring:message code="activity.customer.actions.leave" />' />
									</div>
								</jstl:when>
								<jstl:otherwise>
										<spring:message code="activity.customer.info.activity.full" />
								</jstl:otherwise>
							</jstl:choose>
						</jstl:otherwise>
					</jstl:choose>
				</jstl:when>
				<jstl:otherwise>
					<spring:message code="activity.customer.info.activity.no.gym" />
				</jstl:otherwise>
			</jstl:choose>
			</jstl:if>
			<jstl:if test="${row.isCanceled == true}">
					<spring:message code="activity.customer.info.activity.cancelled" />
			</jstl:if>
		</display:column>
	</security:authorize>
</display:table>

<security:authorize access="hasRole('MANAGER')">
	<jstl:if test="${create == null || create == false}">
		<h2><b><spring:message code="activity.createInfo" /></b></h2>
		<br/>
	</jstl:if>
</security:authorize>