<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="customer/create.do" modelAttribute="actorForm">



	<b><spring:message code="customer.PersonalData" /></b>

	<br />

	<acme:textbox code="customer.name" path="name" />

	<acme:textbox code="customer.surname" path="surname" />

	<acme:textbox code="customer.email" path="email" />
	
	<div style="overflow: hidden">
		<div class="inline">
	<acme:textbox code="customer.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="customer.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />


	<acme:textbox code="customer.city" path="city"/>
	
	<acme:textbox code="customer.country" path="country"/>
	
	<br />

	<!-- Usuario y contraseņa -->

	<b><spring:message code="customer.LoginData" /></b>

	<br />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="customer.username" path="username" />
		</div>
		<jstl:if test="${duplicate != null}">
			<div>
				<span class="message"><spring:message code="${duplicate}" /></span>
			</div>
		</jstl:if>
	</div>

	<acme:password code="customer.password" path="password" />

	<div style="overflow: hidden">
		<div class="inline">
			<acme:password code="customer.secondPassword" path="secondPassword" />
		</div>
		<jstl:if test="${pass != null}">
			<div>
				<span class="message"><spring:message code="${pass}" /></span>
			</div>
		</jstl:if>
	</div>

	<br />

	<!-- Aceptar para continuar -->

	<form:label path="checkBox">
		<spring:message code="customer.checkBox" />
	</form:label>
	<form:checkbox path="checkBox" />
	<a href="misc/terms.do"> <spring:message
			code="customer.moreInfo" />
	</a>
	<form:errors class="error" path="checkBox" />

	<br />
	<br />

	<!-- Acciones -->
	<acme:submit name="save" code="customer.signIn"/>
	
	<acme:cancel url="" code="customer.cancel"/>

</form:form>

<br>

<!-- Errores -->

<jstl:if test="${terms != null}">
	<span class="message"><spring:message code="${terms}" /></span>
</jstl:if>