<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<form:form action="customer/edit.do" modelAttribute="customer">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="userAccount" />

	<!-- Campos obligatorios a rellenar -->

	<b><spring:message code="customer.PersonalData" /></b>

	<br />

	<acme:textbox code="customer.name" path="name" />

	<acme:textbox code="customer.surname" path="surname" />

	<acme:textbox code="customer.email" path="email" />
	
	<div style="overflow: hidden">
		<div class="inline">
	<acme:textbox code="customer.phone" path="phone" placeholder="+XX (YYY) ZZZZ"/>
		</div>
		<jstl:if test="${phone != null}">
			<div>
				<span class="message"><spring:message code="${phone}" /></span>
			</div>
		</jstl:if>
	</div>
	
	<div style="overflow: hidden">
		<div class="inline">
			<acme:textbox code="customer.postalAddress" path="postalAddress"
				placeholder="Ej: 41010" />
		</div>
		<jstl:if test="${postal != null}">
			<div>
				<span class="message"><spring:message code="${postal}" /></span>
			</div>
		</jstl:if>
	</div>
	<br />
	
	<acme:textbox code="customer.city" path="city" />
	
	<acme:textbox code="customer.country" path="country" />

	<br />
	
	

	<!-- Acciones -->
	
	<acme:submit name="save" code="customer.save"/>
	
	<acme:cancel url="" code="customer.cancel"/>

</form:form>

<br>