
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Customer;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import forms.ActorForm;
import services.ManagerService;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ManagerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService managerService;
	@Autowired
	private TrainerService trainerService;
	@Autowired
	private GymService gymService;

	// Templates --------------------------------------------------------------

	// An actor who is authenticated as a manager must be able to: Associate a trainer with one of his or her gyms.
	// Comprobamos que se puede editar un customer correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer trainerId, Integer gymId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#associateTrainerToGym");
			Trainer trainer = this.trainerService.findOne(trainerId);
			Gym gym = this.gymService.findOne(gymId);
			trainer.setGym(gym);
			Trainer trainerSaved = this.trainerService.saveAndFlush(trainer);
			Assert.isTrue(trainerSaved != null && trainerSaved.getId() != 0);

			System.out.println(trainerSaved.getName() + "-" + trainerSaved.getEmail());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	
	/*
	 * An actor who is not authenticated must be able to: Register to the system as a manager or a customer.
	 *
	 * En este caso de uso se llevara a cabo el registro de un manager en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerManager(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String city, final String country, final String newUsername, final String password,
		final String secondPassword, final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setCity(city);
			actor.setCountry(country);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Manager manager = this.managerService.reconstruct(actor);

			//Comprobamos atributos
			this.managerService.comprobacion(manager);

			//Guardamos
			this.managerService.saveForm(manager);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerManagerDriver() {

		final Object testingData[][] = {
			// Creaci�n de manager como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como autentificado (2) -> false
			{
				"customer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como autentificado (3) -> false
			{
				"trainer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager como autentificado (4) -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "city", "country", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de manager con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "customer1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "city", "country", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de manager con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username8", "password1", "password1", true, null
			},
			// Creaci�n de manager con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "city", "country", "username9", "password1", "password1", true, null
			},
			// Creaci�n de manager con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "city", "country", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerManager((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
	}
	
	@Test
	public void associateTrainerToGym() {
		
		List<Trainer> trainers = (List<Trainer>) trainerService.findAll();
		Trainer trainer1 = trainers.get(0);
		
		List<Gym> gyms = (List<Gym>) gymService.findAll();
		Gym gym1 = gyms.get(0);
		
		final Object testingData[][] = {
			{
				"manager1", trainer1.getId(), gym1.getId(), null
			}, {
				"manager1", null, null, NullPointerException.class
			}, {
				"manager1", 0, 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Class<?>) testingData[i][3]);
	}

}
