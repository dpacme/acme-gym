
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Customer;
import forms.ActorForm;
import services.CustomerService;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CustomerService customerService;

	// Templates --------------------------------------------------------------


	// An actor who is authenticated must be able to: Edit his or her personal data.
	// Comprobamos que se puede editar un customer correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer customerId, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#editCustomer");
			Customer customer = this.customerService.findOne(customerId);
			customer.setName("Name Modified");
			customer.setEmail("EmailModified@GMAIL.COM");
			Customer customerSaved = this.customerService.saveAndFlush(customer);
			Assert.isTrue(customerSaved != null && customerSaved.getId() != 0);

			System.out.println(customerSaved.getName() + "-" + customerSaved.getEmail());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	
	/*
	 * An actor who is not authenticated must be able to: Register to the system as a manager or a customer.
	 *
	 * En este caso de uso se llevara a cabo el registro de un customer en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 * � Contrase�as no coinciden
	 */
	public void registerCustomer(final String username, final String name, final String surname, final String email, final String phone, final String postalAddress, final String city, final String country, final String newUsername, final String password,
		final String secondPassword, final Boolean checkBox, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);

			// Inicializamos los atributos para la creaci�n
			final ActorForm actor = new ActorForm();

			actor.setName(name);
			actor.setSurname(surname);
			actor.setEmail(email);
			actor.setPhone(phone);
			actor.setPostalAddress(postalAddress);
			actor.setCity(city);
			actor.setCountry(country);

			actor.setUsername(newUsername);
			actor.setPassword(password);
			actor.setSecondPassword(secondPassword);

			actor.setCheckBox(checkBox);

			//Reconsturimos
			final Customer customer = this.customerService.reconstruct(actor);

			//Comprobamos atributos
			this.customerService.comprobacion(customer);

			//Guardamos
			this.customerService.saveForm(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void registerCustomerDriver() {

		final Object testingData[][] = {
			// Creaci�n de customer como autentificado (1) -> false
			{
				"admin", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer como autentificado (2) -> false
			{
				"customer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username2", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer como autentificado (3) -> false
			{
				"trainer1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer como autentificado (4) -> false
			{
				"manager1", "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username3", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer con postalAddress incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "56118916511", "city", "country", "username4", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer sin aceptar t�rminos -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username5", "password1", "password1", false, IllegalArgumentException.class
			},
			// Creaci�n de customer con usuario no �nico -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "customer1", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer con contrase�as no coincidentes -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username7", "password1", "password2", true, IllegalArgumentException.class
			},
			// Creaci�n de customer con tel�fono incorrecto -> false
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "iuyvicuy", "41010", "city", "country", "username10", "password1", "password1", true, IllegalArgumentException.class
			},
			// Creaci�n de customer con todo correcto -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "41010", "city", "country", "username8", "password1", "password1", true, null
			},
			// Creaci�n de customer con codigopostal vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "+34 (123) 1234", "", "city", "country", "username9", "password1", "password1", true, null
			},
			// Creaci�n de customer con telefono vacio -> true
			{
				null, "NameTest1", "SurnameTest1", "email@domain.com", "", "41010", "city", "country", "username13", "password1", "password1", true, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerCustomer((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (String) testingData[i][10], (Boolean) testingData[i][11], (Class<?>) testingData[i][12]);
	}

	@Test
	public void editCustomerDriver() {

		List<Customer> customers = (List<Customer>) this.customerService.findAll();
		final Object testingData[][] = {
			{
				"customer1", customers.get(0).getId(), null
			}, {
				"customer1", null, NullPointerException.class
			}, {
				"customer1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
