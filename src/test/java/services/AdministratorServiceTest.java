
package services;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Customer;
import domain.Gym;
import domain.Manager;
import domain.Trainer;
import forms.ActorForm;
import services.ManagerService;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class AdministratorServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private ManagerService managerService;
	@Autowired
	private TrainerService trainerService;
	@Autowired
	private AdministratorService administratorService;

	// Templates --------------------------------------------------------------

	// An actor who is authenticated as an administrator must be able to: Ban or unban a manager. Banning him or her means that his or her account is disa-bled, which prevents him or her from logging in to the system.
	// Comprobamos que se puede editar un customer correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos o incorrectos.
	// Test positivo y 2 tests negativos

	protected void template2(String username, Integer managerId, String ban, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			this.authenticate(username);
			System.out.println("#banUnbanManager");
			Manager manager = this.managerService.findOne(managerId);
			if(ban == "ban")
				manager.setIsBanned(true);
			else 
				manager.setIsBanned(false);
			Manager managerSaved = this.managerService.saveAndFlush(manager);
			Assert.isTrue(managerSaved != null && managerSaved.getId() != 0);

			System.out.println(managerSaved.getName() + "-" + managerSaved.getEmail());

			this.unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
	

	// Drivers ----------------------------------------------------------------------

	@Test
	public void banUnbanManager() {
		List<Manager> managers = (List<Manager>) managerService.findAll();
		Manager manager1 = managers.get(0);
		Manager manager2 = managers.get(0);
		
		final Object testingData[][] = {
			{
				"admin", manager1.getId(), "ban", null
			},{
				"admin", manager2.getId(), "unban", null
			}, {
				"admin", null, null, NullPointerException.class
			}, {
				"admin", 0, "ban", IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (Class<?>) testingData[i][3]);
	}

}
